import pytest
from pynndb import Manager, Doc, transparent_resize, WriteTransaction
from lmdb import MapFullError
from shutil import rmtree

DATABASE = '.database'


def test_basic_overflow():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'map_size': 1024 * 32, 'auto_resize': True})
    people = db.table('people')
    with pytest.raises(MapFullError):
        for x in range(2):
            with WriteTransaction(db) as txn:
                people.append(Doc({'Name': 'Fred'}), txn=txn)

    with pytest.raises(MapFullError):
        with WriteTransaction(db) as txn:
            for x in range(100):
                people.append(Doc({'Name': 'Fred'}), txn=txn)

    @transparent_resize
    def extend_it(db, txn=None):
        for x in range(1000):
            people.append(Doc({'Name': 'Fred'}), txn=txn)

    extend_it(db)


def test_overflow_while_opening():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'map_size': 1024 * 32, 'auto_resize': True})
    people = db.table('people')
    with pytest.raises(MapFullError):
        for x in range(76):
            with WriteTransaction(db) as txn:
                people.append(Doc({'name': 'Fred'}), txn=txn)

    @transparent_resize
    def create_index(db, txn=None):
        people.ensure('by_name', '{name}', duplicates=True, txn=txn)

    create_index(db)


def test_overflow_fault_while_opening():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'map_size': 1024 * 32, 'auto_resize': True})
    people = db.table('people')
    with pytest.raises(MapFullError):
        for x in range(52):
            with WriteTransaction(db) as txn:
                people.append(Doc({'name': 'Fred'}), txn=txn)

    @transparent_resize
    def create_index(db, txn=None):
        people.ensure('by_name', '{name}', duplicates=True, txn=txn)

    create_index(db)

    people.close()
    people = db.table('people')

    @transparent_resize
    def extendit(db, txn=None):
        people.ensure('by_name', '{name}|XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', duplicates=True, force=True, txn=txn)

    extendit(db)


def test_basic_small_size():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager().database(DATABASE, config={'map_size': 1024 * 32, 'auto_resize': True})

    @transparent_resize
    def add_2(db, txn=None):
        people = db.table('people', txn=txn)
        for x in range(2):
            people.append(Doc({'Name': 'Fred'}), txn=txn)
        return people
    people = add_2(db)

    with pytest.raises(MapFullError):
        with WriteTransaction(db) as txn:
            people = db.table('people', txn=txn)
            for x in range(10000):
                people.append(Doc({'Name': 'Fred'}), txn=txn)

    @transparent_resize
    def extend_it(db, txn=None):
        for x in range(1000):
            people.append(Doc({'Name': 'Fred'}), txn=txn)

    extend_it(db)
