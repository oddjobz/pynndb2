from unittest import TestCase
from pynndb import Manager, Database
from shutil import rmtree


class UnitTests(TestCase):

    DATABASE = '.database'

    def setUp(self):
        rmtree(self.DATABASE, ignore_errors=True)

    def tearDown(self):
        pass

    def test_manager_basic(self):
        Manager()

    def test_manager_setget(self):
        manager = Manager()
        manager['mydb'].open('.database')
        self.assertTrue('mydb' in manager)

    def test_manager_len(self):
        manager = Manager()
        manager['mydb'].open('.database')
        self.assertEqual(len(manager), 1)

    def test_manager_keys(self):
        manager = Manager()
        manager['mydb'].open('.database')
        self.assertTrue('mydb' in manager.keys())

    def test_manager_items(self):
        manager = Manager()
        manager['mydb'].open('.database')
        for key, val in manager.items():
            self.assertEqual(key, 'mydb')
            self.assertTrue(isinstance(val, Database))

    def test_manager_get_fail(self):
        manager = Manager()
        manager['mydb'].open('.database')
        self.assertFalse('fred' in manager)

    def test_manager_close(self):
        manager = Manager()
        manager['mydb'].open('.database')
        self.assertTrue(len(manager), 1)
        manager['mydb'].close()
        self.assertTrue(len(manager), 0)

    def test_manager_reopen(self):
        manager = Manager()
        manager['mydb'].open('.database')
        self.assertTrue(len(manager), 1)
        manager['mydb'].close()
        self.assertTrue(len(manager), 0)
        manager['mydb'].open('.database')
        self.assertTrue(len(manager), 1)

    def test_database(self):
        manager = Manager()
        db = manager.database('mydb', '.database')
        self.assertTrue('mydb' in manager)
        self.assertEqual(len(manager), 1)
        self.assertTrue('mydb' in manager.keys())
        for key, val in manager.items():
            self.assertEqual(key, 'mydb')
            self.assertTrue(isinstance(val, Database))
        self.assertFalse('fred' in manager)
        db.close()
        self.assertTrue(len(manager), 0)
        db = manager.database('mydb', '.database')
        self.assertTrue(len(manager), 1)

    def test_database_configure(self):
        manager = Manager()
        config = {
            'map_size': 1024 * 1024
        }
        db = manager.database('mydb', '.database', config)
        self.assertEqual(db.map_size, 1024 * 1024)
        db.close()
        db = manager.database('mydb', '.database')
        self.assertEqual(db.map_size, 1024 * 1024)

    def test_manager_db_repr(self):
        manager = Manager()
        db = manager['mydb'].open('.database')
        self.assertEqual('<pynndb.database.Database instance> path=".database" status=open', str(db))

    def test_manager_db_sync(self):
        manager = Manager()
        db = manager['mydb'].open('.database')
        db.sync(False)
        db.sync(True)

    def test_double_open(self):
        manager = Manager()
        db1 = manager['mydb'].open('.database')
        db2 = db1.open('.database')
        self.assertEqual(db1, db2)

    def test_double_close(self):
        manager = Manager()
        db = manager['mydb'].open('.database')
        db.close()
        self.assertFalse(db.isopen)

    def test_storage_used(self):
        db = Manager().database('.database')
        # Now we have a UUID too!
        self.assertEqual(db.storage_used(), (12288, [('__metadata__', 12288)]))
        self.assertEqual(db.storage_allocated, 2147483648)
