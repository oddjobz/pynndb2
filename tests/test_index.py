from unittest import TestCase
from pynndb import Manager, Doc, Index, DuplicateKey, IndexAlreadyExists, \
    WriteTransaction, NoSuchIndex, DocumentDoesntExist
from subprocess import call


class UnitTests(TestCase):

    DATABASE = '.database'

    def setUp(self):
        call(['rm', '-rf', self.DATABASE])
        self.data = [
            {'name': 'Tom', 'age': 20},
            {'name': 'Dick', 'age': 30},
            {'name': 'Harry', 'age': 25},
            {'name': 'Sam', 'age': 35},
            {'name': 'Sally', 'age': 21},
        ]

    def tearDown(self):
        pass

    def setup_people(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}', duplicates=True)
        people.ensure('by_age', '{age}', duplicates=True)
        self.load(people)
        self.assertEqual(people['by_name'].records(), 5)
        self.assertEqual(people['by_age'].records(), 5)
        self.manager = manager
        return people

    def load(self, people):
        with WriteTransaction(people._database) as txn:
            for person in self.data:
                doc = Doc(person)
                people.append(doc, txn=txn)
                person['_id'] = doc.key

    def test_ensure_basic(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        manager['mydb']['people'].open()
        manager['mydb']['people'].ensure('by_name', '{name}')
        self.assertEqual(list(manager['mydb']['people']), ['by_name'])
        self.assertEqual(list(manager['mydb']['people'].indexes()), ['by_name'])
        manager['mydb'].close()
        manager['mydb'].open(self.DATABASE)
        manager['mydb']['people'].open()
        manager['mydb']['people'].ensure('by_name', '{name}')
        self.assertEqual(list(manager['mydb']['people'].indexes()), ['by_name'])

    def test_ensure_index(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')
        for person in self.data:
            people.append(Doc(person))

        self.assertEqual([p._name for p in people.find('by_name')], ['Dick', 'Harry', 'Sally', 'Sam', 'Tom'])
        self.assertEqual([p._age for p in people.find('by_age')], [20, 21, 25, 30, 35])

    def test_delete(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')

        for person in self.data:
            doc = Doc(person)
            people.append(doc)
            person['_id'] = doc.oid

        people.delete(self.data[0]['_id'])

        names = [p._name for p in people.find()]
        names.sort()
        self.assertEqual(names, ['Dick', 'Harry', 'Sally', 'Sam'])
        self.assertEqual([p._name for p in people.find('by_name')], ['Dick', 'Harry', 'Sally', 'Sam'])
        self.assertEqual([p._age for p in people.find('by_age')], [21, 25, 30, 35])

    def test_save(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')

        for person in self.data:
            doc = Doc(person)
            people.append(doc)

        names = [p._name for p in people.find()]
        names.sort()
        self.assertEqual(names, ['Dick', 'Harry', 'Sally', 'Sam', 'Tom'])
        self.assertEqual([p._name for p in people.find('by_name')], ['Dick', 'Harry', 'Sally', 'Sam', 'Tom'])
        self.assertEqual([p._age for p in people.find('by_age')], [20, 21, 25, 30, 35])

        for person in people.find():
            person._name = person._name[::-1]
            people.save(person)

        self.assertEqual([p._name for p in people.find('by_name')], ['kciD', 'maS', 'moT', 'yllaS', 'yrraH'])
        self.assertEqual([p._age for p in people.find('by_age')], [20, 21, 25, 30, 35])

    def test_range_all_incl(self):
        people = self.setup_people()
        lower = Doc(self.data[1])
        upper = Doc(self.data[0])
        self.assertEqual(
            [doc._name for doc in people.range('by_name', lower=lower, upper=upper)],
            ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
        )

    def test_range_all_incl_with_transaction(self):
        people = self.setup_people()
        lower = Doc(self.data[1])
        upper = Doc(self.data[0])
        with people.read_transaction as txn:
            self.assertEqual(
                [doc._name for doc in people.range('by_name', lower=lower, upper=upper, txn=txn)],
                ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
            )

    def test_range_all(self):
        people = self.setup_people()
        lower = Doc(self.data[1])
        upper = Doc(self.data[0])
        self.assertEqual(
            [doc._name for doc in people.range('by_name', lower=lower, upper=upper, inclusive=False)],
            ['Harry', 'Sally', 'Sam']
        )

    def test_range_no_lower_incl(self):
        people = self.setup_people()
        upper = Doc(self.data[0])
        self.assertEqual(
            [doc._name for doc in people.range('by_name', upper=upper)],
            ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
        )

    def test_range_no_lower(self):
        people = self.setup_people()
        upper = Doc(self.data[0])
        self.assertEqual(
            [doc._name for doc in people.range('by_name', upper=upper, inclusive=False)],
            ['Dick', 'Harry', 'Sally', 'Sam']
        )

    def test_range_no_upper(self):
        people = self.setup_people()
        lower = Doc(self.data[1])
        self.assertEqual(
            [doc._name for doc in people.range('by_name', lower=lower, inclusive=False)],
            ['Harry', 'Sally', 'Sam', 'Tom']
        )

    def test_range_no_upper_excl(self):
        people = self.setup_people()
        lower = Doc(self.data[1])
        self.assertEqual(
            [doc._name for doc in people.range('by_name', lower=lower, inclusive=True)],
            ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
        )

    def test_range_no_range_excl(self):
        people = self.setup_people()
        self.assertEqual(
            [doc._name for doc in people.range('by_name', inclusive=False)],
            ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
        )

    def test_range_keyonly(self):
        people = self.setup_people()
        sorted_data = sorted(self.data, key=lambda doc: doc['name'])
        self.assertEqual(
            [cursor.val for cursor in people.range('by_name', keyonly=True)],
            [item['_id'] for item in sorted_data]
        )

    def test_seek_one(self):
        people = self.setup_people()
        del self.data[0]['_id']
        self.assertEqual(people.seek_one('by_name', Doc({'name': 'Tom'})).doc, self.data[0])

    def test_seek_name(self):
        people = self.setup_people()
        del self.data[0]['_id']
        del self.data[3]['_id']
        self.assertEqual([doc.doc for doc in people.seek('by_name', Doc({'name': 'Tom'}))], [self.data[0]])
        self.assertEqual([doc.doc for doc in people.seek('by_name', Doc({'name': 'Sam'}))], [self.data[3]])

    def test_seek_age(self):
        people = self.setup_people()
        for item in self.data:
            del item['_id']
        self.assertEqual([doc.doc for doc in people.seek('by_age', Doc({'age': 20}), limit=1)], [self.data[0]])

    def test_seek_name_dups(self):
        people = self.setup_people()
        for p in self.data:
            del p['_id']
        people.ensure('by_age', '{age}', duplicates=True, force=True)
        self.load(people)
        del self.data[0]['_id']
        del self.data[3]['_id']
        self.assertEqual(
            [doc.doc for doc in people.seek('by_name', Doc({'name': 'Tom'}))],
            [self.data[0], self.data[0]]
        )
        self.assertEqual(
            [doc.doc for doc in people.seek('by_name', Doc({'name': 'Sam'}))],
            [self.data[3], self.data[3]]
        )

    def test_drop_table(self):
        self.setup_people()
        self.assertEqual(list(self.manager['mydb'].keys()), ['__metadata__', 'people'])
        t1 = self.manager['mydb']['test1'].open()
        t2 = self.manager['mydb']['test2'].open()
        t1.append(Doc({'name': 't1'}))
        t2.append(Doc({'name': 't2'}))
        t1.ensure('by_name', '{name}', duplicates=True)
        t2.ensure('by_name', '{name}', duplicates=True)
        self.assertEqual(
            list(self.manager['mydb'].tables(all=True)),
            ['__metadata__', '_people_by_age', '_people_by_name', '_test1_by_name', '_test2_by_name',
                'people', 'test1', 'test2']
        )
        self.manager['mydb'].drop('people')
        self.assertEqual(list(self.manager['mydb'].keys()), ['__metadata__', 'test1', 'test2'])
        self.assertEqual(
            list(self.manager['mydb'].tables(all=True)),
            ['__metadata__', '_test1_by_name', '_test2_by_name', 'test1', 'test2']
        )

    def test_index_attributes(self):
        data = [
            {'name': 'Tom', 'age': 20, 'tags': ['red', 'green', 'blue', 'yellow', 'cyan']},
            {'name': 'Dick', 'age': 30, 'tags': ['red', 'green', 'blue']},
            {'name': 'Harry', 'age': 25, 'tags': ['yellow', 'cyan']},
            {'name': 'Sam', 'age': 35, 'tags': ['red', 'blue', 'yellow']},
            {'name': 'Sally', 'age': 21, 'tags': ['green', 'blue', 'purple']},
        ]
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_tags', 'def func(doc): return [tag.encode() for tag in doc["tags"]]', duplicates=True)
        for person in data:
            doc = Doc(person)
            people.append(doc)

        self.assertEqual(people['by_tags'].records(), 16)
        self.assertEqual(
            [doc._name for doc in people.seek('by_tags', Doc({'tags': ['yellow']}))],
            ['Tom', 'Harry', 'Sam']
        )
        self.assertEqual([doc._name for doc in people.seek('by_tags', Doc({'tags': ['purple']}))], ['Sally'])
        self.assertEqual(
            [doc._name for doc in people.seek('by_tags', Doc({'tags': ['yellow', 'purple']}))],
            ['Tom', 'Harry', 'Sam', 'Sally']
        )

        people.append(Doc({'name': 'Fred', 'tags': ['black']}))
        self.assertEqual([doc._name for doc in people.seek('by_tags', Doc({'tags': ['black']}))], ['Fred'])
        self.assertEqual(people['by_tags'].records(), 17)

        tom = people.seek_one('by_name', Doc({'name': 'Tom'}))
        tom._tags = []
        people.save(tom)

        self.assertEqual(people['by_tags'].records(), 12)
        self.assertEqual([doc._name for doc in people.seek('by_tags', Doc({'tags': ['yellow']}))], ['Harry', 'Sam'])

    def test_ensure(self):
        people = self.setup_people()
        people.ensure('by_name', '{name}', duplicates=True, force=True)
        people.ensure('by_age', '{age}', force=True)
        with self.assertRaises(DuplicateKey):
            self.load(people)
        self.assertEqual(people['by_name'].records(), 5)
        self.assertEqual(people['by_age'].records(), 5)
        people.ensure('by_age', '{age}', force=True, duplicates=True)
        self.load(people)
        self.assertEqual(people['by_name'].records(), 10)
        self.assertEqual(people['by_age'].records(), 10)

    def test_range_none(self):
        people = self.setup_people()
        docs = [doc for doc in people.range('by_name', Doc({'name': 'zz'}), Doc({'name': 'zz'}))]
        self.assertEqual(docs, [])

    def test_empty(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')
        for person in self.data:
            doc = Doc(person)
            people.append(doc)

        self.assertEqual(people.records(), 5)
        people.empty()
        self.assertEqual(people.records(), 0)

        self.assertEqual(len(people), 2)
        self.assertEqual(list(people.keys()), ['by_name', 'by_age'])

        people.drop('by_name')

        self.assertEqual(len(people), 1)
        self.assertEqual(list(people.keys()), ['by_age'])

        people.drop('by_age')

        self.assertEqual(len(people), 0)
        self.assertEqual(list(people.keys()), [])

        people.ensure('by_name', '{name}', duplicates=True)
        people.ensure('by_age', '{age}')
        self.assertEqual(len(people), 2)
        self.assertEqual(list(people.keys()), ['by_name', 'by_age'])

        self.assertEqual(people['by_name'].records(), 0)
        self.assertEqual(people['by_age'].records(), 0)

    def test_reindex(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')
        for person in self.data:
            doc = Doc(person)
            people.append(doc)

        self.assertEqual(people.records(), 5)

        self.assertEqual(len(people), 2)
        self.assertEqual(list(people.keys()), ['by_name', 'by_age'])

        people.drop('by_name')

        self.assertEqual(len(people), 1)
        self.assertEqual(list(people.keys()), ['by_age'])

        people.drop('by_age')

        self.assertEqual(len(people), 0)
        self.assertEqual(list(people.keys()), [])

        people.ensure('by_name', '{name}', duplicates=True)
        people.ensure('by_age', '{age}')
        self.assertEqual(len(people), 2)
        self.assertEqual(list(people.keys()), ['by_name', 'by_age'])

        self.assertEqual(people['by_name'].records(), 5)
        self.assertEqual(people['by_age'].records(), 5)

        self.assertEqual([p._name for p in people.find('by_name')], ['Dick', 'Harry', 'Sally', 'Sam', 'Tom'])
        self.assertEqual([p._age for p in people.find('by_age')], [20, 21, 25, 30, 35])

        with self.assertRaises(NoSuchIndex):
            people.reindex('no index')

    def test_index_change(self):
        people = self.setup_people()
        self.assertEqual(people['by_name'].records(), 5)
        self.assertEqual(people['by_age'].records(), 5)
        doc = people.first()
        doc._age = 99
        people.save(doc)
        self.assertEqual(people['by_name'].records(), 5)
        self.assertEqual(people['by_age'].records(), 5)

        ages = [doc.key for doc in people.range('by_age', keyonly=True)]
        self.assertEqual(ages, [b'21', b'25', b'30', b'35', b'99'])

    def test_save_2(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')

        for person in self.data:
            doc = Doc(person)
            people.append(doc)

        for person in people.find():
            del person['name']
            people.save(person)

        self.assertEqual(people['by_name'].records(), 0)
        self.assertEqual(people['by_age'].records(), 5)

        with self.assertRaises(DocumentDoesntExist):
            person.oid = None
            people.save(person)

    def test_put(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')

        doc = Doc({'xxx': 'yyy'})
        people.append(doc)

    def test_put_index_write_error(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')

        doc = Doc({'name': 'Fred'})
        people.append(doc)
        with self.assertRaises(DuplicateKey):
            people.append(doc)

        doc.oid = None
        with self.assertRaises(DuplicateKey):
            people.append(doc)

    def test_add_dup_index(self):
        manager = Manager()
        db = manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        people.ensure('by_name', '{name}')
        people.ensure('by_age', '{age}')
        with self.assertRaises(IndexAlreadyExists):
            people['by_name'] = Index(db, None, {'func': '     '})

    def test_repr(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        self.assertEqual(str(people), '<pynndb.table.Table instance> name="people" status=open')

    def test_drop_index(self):
        people = self.setup_people()
        people.drop('by_name')
        self.assertEqual(len(people), 1)
        with self.assertRaises(NoSuchIndex):
            people.drop('no index')

    def test_range_no_index(self):
        people = self.setup_people()
        lower = Doc(self.data[1])
        upper = Doc(self.data[0])
        with self.assertRaises(NoSuchIndex):
            [doc._name for doc in people.range('by_xxx', lower=lower, upper=upper)]

    def test_seek_one_no_index(self):
        people = self.setup_people()
        with self.assertRaises(NoSuchIndex):
            people.seek_one('by_xxx', Doc({'name': 'Tom'}))

    def test_seek_no_index(self):
        people = self.setup_people()
        with self.assertRaises(NoSuchIndex):
            [person for person in people.seek('by_xxx', Doc({'name': 'Tom'}))]

    def test_seek_keyonly(self):
        people = self.setup_people()
        self.assertEqual(
            [cursor.val for cursor in people.seek('by_name', Doc({'name': 'Tom'}), keyonly=True)],
            [self.data[0]['_id']]
        )

    def test_duplicate_keyonly(self):
        people = self.setup_people()
        self.load(people)
        #
        #   Get all records with doc._name == "Dick"
        #
        names = [person._name for person in people.seek('by_name', Doc({'name': 'Dick'}))]
        self.assertEqual(names, ['Dick', 'Dick'])
        #
        #   Get all records with doc._name == "Dick", but just the keys and limit the results to 1
        #
        result = list(people.seek('by_name', Doc({'name': 'Dick'}), keyonly=True, limit=1))[0]
        #
        #   Check the cursor.count for the single return is == 2
        #
        self.assertEqual(result.count, 2)
        #
        #   Use the cursor result to fetch the actual data record
        #
        self.assertEqual(result.fetch().doc, {'name': 'Dick', 'age': 30})

    def test_multiple_keys_contrived(self):
        call(['rm', '-rf', self.DATABASE])
        items = [
            {'name': 'Alpha', 'val': [1, 2, 3, 4, 9, 19]},
            {'name': 'Beta', 'val': [2, 3, 4]},
            {'name': 'Gamma', 'val': [3, 4, 8, 18]},
            {'name': 'Omega', 'val': [1, 2]},
            {'name': 'Epsilon', 'val': [1, 20]},
        ]
        db = Manager().database(self.DATABASE)
        data = db.table('data')
        data.ensure('by_name', '{name}', duplicates=True)
        data.ensure('by_val', 'def func(doc): return [f"{val:>3}".encode() for val in doc["val"]]', duplicates=True)

        for item in items:
            data.append(Doc(item))
        self.assertEqual([doc._name for doc in data.find()], ['Alpha', 'Beta', 'Gamma', 'Omega', 'Epsilon'])
        self.assertEqual([doc._name for doc in data.range('by_name')], ['Alpha', 'Beta', 'Epsilon', 'Gamma', 'Omega'])
        #
        #   So this is relatively obvious from above
        #
        oneor20 = ['Alpha', 'Omega', 'Epsilon', 'Epsilon']
        self.assertEqual([doc._name for doc in data.seek('by_val', Doc({'val': [1, 20]}))], oneor20)
        #
        #   This is more complex, nodups, so there will be only 2 results as two keys are going in, one
        #   will represent the first with a '1' and the other, the first with a 20.
        #
        oneor20nodups = ['Alpha', 'Epsilon']
        lower = upper = Doc({'val': [1, 20]})
        self.assertEqual([doc._name for doc in data.range('by_val', lower, upper, nodups=True)], oneor20nodups)

    def test_index_existing(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        self.load(people)
        self.manager = manager
        people.ensure('by_name', '{name}', duplicates=True)
        people.ensure('by_age', '{age}', duplicates=True)
        self.assertEqual(people['by_name'].records(), 5)
        self.assertEqual(people['by_age'].records(), 5)
