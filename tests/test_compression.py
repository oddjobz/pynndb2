from unittest import TestCase
from pynndb import Manager, Doc
from shutil import rmtree
import os
from pynndb import TableNotOpen, CompressionType, TrainingDataExists, WriteTransaction
from ujson import dumps
import pytest

DATABASE = '.database'
COMPRESSIONS = [(CompressionType.ZSTD, 5), (CompressionType.SNAPPY, None), (CompressionType.NONE, None)]


@pytest.fixture
def data():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sally', 'age': 21},
    ]


@pytest.fixture
def db():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager()['mydb'].open(DATABASE)
    yield db
    db.close()


@pytest.fixture(params=COMPRESSIONS)
def people(request, data, db):
    compression_type = request.param[0]
    compression_level = request.param[1]
    result = db['people'].open(compression_type=compression_type, compression_level=compression_level)
    with WriteTransaction(db) as txn:
        [person.update({'_id': result.append(Doc(person), txn=txn).key}) for person in data]
    yield result


def test_zstd_training_from_sample_records(data, db):
    people = db['people'].open()
    with WriteTransaction(db) as txn:
        for i in range(10):
            [person.update({'_id': people.append(Doc(person), txn=txn).key}) for person in data]
    assert people.zstd_train(training_record_count=people.records()) is None, (
        'Failed to acquire training data')


def test_zstd_training_from_samples(data, db):
    people = db['people'].open()
    with WriteTransaction(db) as txn:
        for i in range(10):
            [person.update({'_id': people.append(Doc(person), txn=txn).key}) for person in data]

    samples = []
    for i in range(10):
        for sample in data:
            samples.append(dumps(sample).encode())
    assert people.zstd_train(training_samples=samples) is None, (
        'Failed to acquire training data')


def test_zstd_training_conflicting_params(data, db):
    with pytest.raises(ValueError):
        people = db['people'].open()
        people.zstd_train(training_samples=['1'], training_record_count=1)


class UnitTests(TestCase):

    DATABASE = '.database'

    def setUp(self):
        rmtree(self.DATABASE, ignore_errors=True)
        self.data = [
            {'name': 'Tom', 'age': 20},
            {'name': 'Dick', 'age': 30},
            {'name': 'Harry', 'age': 25},
            {'name': 'Sam', 'age': 35},
            {'name': 'Sally', 'age': 21},
        ]
        self.cdata = [
            {'name': 'key1', 'data': 'data1 data1 data1 data1 data1 data1 data1 data1 data1 data1 data1'},
            {'name': 'key2', 'data': 'data1 data1 data1 data1 data1 data1 data1 data1 data1 data1 data1'},
            {'name': 'key3', 'data': 'data1 data1 data1 data1 data1 data1 data1 data1 data1 data1 data1'},
        ]

    def tearDown(self):
        pass

    def setup_people(self):
        manager = Manager()
        manager['mydb'].open(self.DATABASE)
        people = manager['mydb']['people'].open()
        self.load(people)
        self.manager = manager
        return people

    def load(self, people):
        with WriteTransaction(people._database) as txn:
            for person in self.data:
                doc = Doc(person)
                people.append(doc, txn=txn)
                person['_id'] = doc.key

    def test_open_compression(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open()
        self.assertFalse(people.compressed())

    def test_compress_unknown(self):
        with self.assertRaises(Exception):
            Manager()['mydb'].open(self.DATABASE)['people'].open(compression_type='z')

    def test_compress_unknown_2(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open()
        self.assertEqual(people.compression_type(), CompressionType.NONE)

    def test_compress_zstd(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.ZSTD, 5)
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.ZSTD)

    def test_compress_snappy(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.SNAPPY, 5)
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.SNAPPY)

    def test_compress_zstd_data_notopen(self):
        people = Manager()['mydb'].open(self.DATABASE)['people']
        with self.assertRaises(TableNotOpen):
            people.zstd_train(people.records())

    def test_compress_zstd_data(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open()
        for i in range(20):
            self.load(people)
        people.zstd_train(people.records() + 1)
        people.close()

        people.open(CompressionType.ZSTD, 22)
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.ZSTD)

        for i in range(20000):
            self.load(people)
        st = os.stat(f'{self.DATABASE}/data.mdb')
        size1 = st.st_blocks * 512 / 1024 / 1024

        rmtree(self.DATABASE, ignore_errors=True)

        people = Manager()['mydb'].open(self.DATABASE)['people']
        people.open()
        for i in range(20):
            self.load(people)

        self.assertFalse(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.NONE)

        for i in range(20000):
            self.load(people)
        st = os.stat(f'{self.DATABASE}/data.mdb')
        size2 = st.st_blocks * 512 / 1024 / 1024

        self.assertGreater(size2 - size1, 2)

    def test_compress_snappy_data(self):
        db = Manager()['mydb'].open(self.DATABASE)
        data = db['people'].open(CompressionType.SNAPPY)

        self.assertTrue(data.compressed())
        self.assertEqual(data.compression_type(), CompressionType.SNAPPY)

        for i in range(20000):
            with WriteTransaction(db) as txn:
                for d in self.cdata:
                    doc = Doc(d)
                    data.append(doc, txn=txn)
                    d['_id'] = doc.key

        st = os.stat(f'{self.DATABASE}/data.mdb')
        size1 = st.st_blocks * 512 / 1024 / 1024

        rmtree(self.DATABASE, ignore_errors=True)

        db = Manager()['mydb'].open(self.DATABASE)
        data = db['people'].open()
        for i in range(20000):
            with WriteTransaction(db) as txn:
                for d in self.cdata:
                    doc = Doc(d)
                    data.append(doc, txn=txn)
                    d['_id'] = doc.key

        st = os.stat(f'{self.DATABASE}/data.mdb')
        size2 = st.st_blocks * 512 / 1024 / 1024

        self.assertGreater(size2 - size1, 3)

    def test_compress_test_reopen(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.ZSTD, 3)
        for i in range(5000):
            self.load(people)
        people.open()

    def test_compress_test_open_unknown(self):
        db = Manager()['mydb'].open(self.DATABASE)
        people = db['people'].open(CompressionType.ZSTD, 3)
        for i in range(1):
            self.load(people)
        people.close()
        people = db['people'].open(CompressionType.ZSTD, 3)

    def test_compress_test_bad_training(self):
        db = Manager()['mydb'].open(self.DATABASE)
        people = db['people'].open()
        for i in range(1):
            self.load(people)
        result = people.zstd_train(0)
        self.assertEqual(str(result), 'cannot train dict: Src size is incorrect')

    def test_compress_retrain_more(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open()
        for i in range(5000):
            self.load(people)
        people.zstd_train(10)
        people.close()
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.ZSTD)
        for doc in people.find(limit=1):
            self.assertEqual(doc.doc, {'name': 'Tom', 'age': 20})

        people.close()
        people = Manager()['mydb'].open(self.DATABASE)['people'].open()
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.ZSTD)
        for doc in people.find(limit=1):
            self.assertEqual(doc.doc, {'name': 'Tom', 'age': 20})

    def test_compress_retrain_less(self):
        #
        #   Write 5000 records with no compression
        #
        db = Manager().database('mydb', self.DATABASE)
        people = db.table('people')
        #
        for i in range(5000):
            self.load(people)
        #
        #   Train on the first 10 records
        #
        people.zstd_train(10)
        people.close()
        # db.close()

        # db = Manager()['mydb'].open(self.DATABASE)
        #
        #   Open with compression and training dict
        #
        people = db['people'].open(compression_type=CompressionType.ZSTD, compression_level=3)
        #
        #   Check we used the dict!
        #
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.ZSTD)
        for doc in people.find(limit=1):
            self.assertEqual(doc.doc, {'name': 'Tom', 'age': 20})
        with self.assertRaises(TrainingDataExists):
            people.zstd_train(0)
        people.close()

        # db = Manager()['mydb'].open(self.DATABASE)
        people = db['people'].open()
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.ZSTD)
        for doc in people.find(limit=1):
            self.assertEqual(doc.doc, {'name': 'Tom', 'age': 20})

        people.empty()
        for i in range(100):
            self.load(people)
        for doc in people.find(limit=1):
            self.assertEqual(doc._name, 'Tom')
            self.assertEqual(doc._age, 20)

    def test_compress_drop(self):
        db = Manager()['mydb'].open(self.DATABASE)
        people = db['people'].open()
        for i in range(100):
            self.load(people)
        people.zstd_train(10)
        people.close()
        people.open(CompressionType.ZSTD, 22)

        self.assertEqual([key[0] for key in people._meta.fetch(people.name)], ['_people!zstd_cdict', '_people@config'])
        db.drop('people')
        self.assertEqual([key[0] for key in people._meta.fetch(people.name)], [])

    def test_compress_drop_with_index(self):
        db = Manager()['mydb'].open(self.DATABASE)
        people = db['people'].open()
        people.ensure('by_name', '{name}', duplicates=True)
        for i in range(100):
            self.load(people)
        people.zstd_train(10)
        people.close()
        people.open(CompressionType.ZSTD, 22)

    def test_write_then_compress(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open()
        for i in range(5):
            self.load(people)
        people.zstd_train(10)
        people.close()
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
        self.assertTrue(people.compressed())
        self.assertEqual(people.compression_type(), CompressionType.ZSTD)
        for doc in people.find(limit=1):
            self.assertEqual(doc.doc, {'name': 'Tom', 'age': 20})

    def test_range_all_incl2(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
        people.ensure('by_name', '{name}', duplicates=True)
        self.load(people)
        lower = Doc(self.data[1])
        upper = Doc(self.data[0])
        self.assertEqual(
            [doc._name for doc in people.range('by_name', lower=lower, upper=upper)],
            ['Dick', 'Harry', 'Sally', 'Sam', 'Tom']
        )

    def test_seek_one(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
        people.ensure('by_name', '{name}', duplicates=True)
        self.load(people)
        del self.data[0]['_id']
        self.assertEqual(people.seek_one('by_name', Doc({'name': 'Tom'})).doc, self.data[0])

    def test_seek_name(self):
        people = Manager()['mydb'].open(self.DATABASE)['people'].open(CompressionType.ZSTD, 22)
        people.ensure('by_name', '{name}', duplicates=True)
        self.load(people)
        del self.data[0]['_id']
        del self.data[3]['_id']
        self.assertEqual([doc.doc for doc in people.seek('by_name', Doc({'name': 'Tom'}))], [self.data[0]])
        self.assertEqual([doc.doc for doc in people.seek('by_name', Doc({'name': 'Sam'}))], [self.data[3]])

