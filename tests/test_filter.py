import pytest
from pynndb import Manager, Doc, NoSuchIndex, WriteTransaction, SerialiserType
from shutil import rmtree
from struct import pack, unpack

DATABASE = '.database'
PAGE_SIZES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
INDEXES = [None, 'by_name', 'by_name_dups']


@pytest.fixture(params=[SerialiserType.UJSON, SerialiserType.ORJSON, SerialiserType.NONE])
def codec(request):
    return request.param


@pytest.fixture
def required_dups():
    return [
        ('Dick', 1),
        ('Fred', 1),
        ('Harry', 1),
        ('Jane', 3),
        ('Jane', 3),
        ('Jane', 3),
        ('Peter', 1),
        ('Sally', 1),
        ('Sam', 2),
        ('Sam', 2),
        ('Tom', 2),
        ('Tom', 2)
    ]


@pytest.fixture
def required_dups_surpressed():
    return [
        ('Dick', 1),
        ('Fred', 1),
        ('Harry', 1),
        ('Jane', 3),
        ('Peter', 1),
        ('Sally', 1),
        ('Sam', 2),
        ('Tom', 2)
    ]


@pytest.fixture
def required():
    return ['Tom', 'Dick', 'Harry', 'Sam', 'Sally', 'Peter', 'Jane', 'Fred']


@pytest.fixture
def data():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sally', 'age': 21},
        {'name': 'Peter', 'age': 11},
        {'name': 'Jane', 'age': 11},
        {'name': 'Fred', 'age': 75},
    ]


@pytest.fixture
def dupdata():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Tom', 'age': 21},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sam', 'age': 25},
        {'name': 'Sally', 'age': 21},
        {'name': 'Peter', 'age': 11},
        {'name': 'Jane', 'age': 11},
        {'name': 'Jane', 'age': 12},
        {'name': 'Jane', 'age': 10},
        {'name': 'Fred', 'age': 75},
    ]


@pytest.fixture
def db():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager()['mydb'].open(DATABASE)
    yield db
    db.close()


@pytest.fixture
def people(data, db, codec):
    people = db.table('people', codec=codec)
    people.ensure('by_name', '{name}')
    people.ensure('by_name_dups', '{name}', duplicates=True)
    with WriteTransaction(db) as txn:
        for person in data:
            people.append(Doc(person), txn=txn)
    yield people


@pytest.fixture
def duppeople(dupdata, db, codec):
    people = db.table('people', codec=codec)
    people.ensure('by_name_dups', '{name}', duplicates=True)
    with WriteTransaction(db) as txn:
        for person in dupdata:
            people.append(Doc(person), txn=txn)
    yield people


@pytest.fixture(params=PAGE_SIZES)
def page_size(request):
    return request.param


@pytest.fixture(params=INDEXES)
def index(request):
    return request.param


def test_filter_primary_check(people, required):
    with people.read_transaction as txn:
        assert [doc.doc._name for doc in people.filter(txn=txn)] == required


def test_filter_no_such_index(people):
    with people.read_transaction as txn:
        with pytest.raises(NoSuchIndex):
            next(people.filter('no_such_index', txn=txn))


def test_filter_primary(people, index, required):
    if index is not None:
        required.sort()
    with people.read_transaction as txn:
        assert [result.doc._name for result in people.filter(index, txn=txn)] == required


def test_filter_primary_with_lower(people, index, required):
    if index is not None:
        required.sort()
        lower = Doc({'name': required[0]}, people.first().key)
    else:
        lower = people.first()
    with people.read_transaction as txn:
        assert [result.doc._name for result in people.filter(
            index,
            lower=lower,
            txn=txn)] == required


def test_filter_primary_with_upper(people, index, required):
    if index is not None:
        required.sort()
        upper = Doc({'name': required[-1]}, people.last().key)
    else:
        upper = Doc(None, people.last().key)
    with people.read_transaction as txn:
        assert [result.doc._name for result in people.filter(
            index,
            upper=upper, txn=txn)] == required


def test_filter_primary_with_both(people, index, required):
    if index is not None:
        required.sort()
        lower = Doc({'name': required[0]}, people.first().key)
        upper = Doc({'name': required[-1]}, people.last().key)
    else:
        lower = people.first()
        upper = people.last()
    with people.read_transaction as txn:
        assert [result.doc._name for result in people.filter(
            index,
            lower=lower,
            upper=upper,
            txn=txn)] == required


def test_filter_primary_with_both_exclusive(people, index, required):
    if index is not None:
        required.sort()
        lower = Doc({'name': required[0]}, people.first().key)
        upper = Doc({'name': required[-1]}, people.last().key)
    else:
        lower = people.first()
        upper = people.last()
    subset = required[1:-1]
    with people.read_transaction as txn:
        assert [result.doc._name for result in people.filter(
            index,
            lower=lower,
            upper=upper,
            inclusive=False,
            txn=txn)] == subset


def test_filter_primary_with_both_exclusive_filter(people, index):
    required = ['Sam', 'Sally']
    if index is not None:
        required.sort()
        lower = people.first(index)
        upper = people.last(index)
    else:
        lower = Doc(None, people.first().key)
        upper = Doc(None, people.last().key)

    with people.read_transaction as txn:
        assert [result.doc._name for result in people.filter(
            index_name=index,
            lower=lower,
            upper=upper,
            expression=lambda doc: doc['name'].lower().startswith('s'),
            inclusive=False,
            txn=txn)] == required


def test_filter_primary_no_context(people, index, required):
    if index:
        required.sort()
    with people.read_transaction as txn:
        assert [result.doc._name for result in people.filter(index, page_size=1, txn=txn)] == [required[0]]


def test_filter_primary_context_sequence(people, index, page_size, required):
    if index:
        required.sort()
    with people.read_transaction as txn:
        person = None
        while True:
            try:
                person = next(people.filter(index, context=person, page_size=page_size, txn=txn))
                assert person.doc._name == required.pop(0)
            except StopIteration:
                break


def test_filter_primary_context_by_page(people, index, page_size, required):
    if index:
        required.sort()
    with people.read_transaction as txn:
        result = None
        while True:
            results = people.filter(index, page_size=page_size, context=result, txn=txn)
            count = 0
            for result in results:
                count += 1
                assert result.doc._name == required.pop(0)
            if not count:
                break


def test_filter_primary_context_sequence_reverse(people, index, required):
    if index:
        required.sort()
    with people.read_transaction as txn:
        # person = list(people.filter(index, page_size=99, txn=txn))[-1]
        context = None
        while True:
            results = list(people.filter(index, page_size=-1, context=context, txn=txn))
            if not len(results):
                break
            context = results[0]
            assert context.doc._name == required.pop()
            assert len(results) == 1


def test_filter_primary_context_by_page_reverse(people, page_size, index, required):
    if index:
        required.sort()
    with people.read_transaction as txn:
        context = None
        while True:
            results = list(people.filter(index, page_size=page_size * -1, context=context, txn=txn))
            if not len(results):
                break
            context = results[0]
            while len(results):
                res = results.pop()
                req = required.pop()
                assert req == res.doc._name, f'page failed, page_size={page_size} found={res.doc._name} expected={req}'


def test_filter_page_back_past_lower(people, index):
    if index is not None:
        name = 'Fred'
        lower = Doc({'name': name})
    else:
        name = 'Dick'
        lower = people.seek_one('by_name', Doc({'name': name}))
    with people.read_transaction as txn:
        context = next(people.filter(index, lower=lower, txn=txn))
        for result in people.filter(index, lower=lower, page_size=-2, context=context, txn=txn):
            pass
        assert result.doc._name == name


def test_filter_suppress_duplicates(duppeople, required_dups_surpressed):
    index = 'by_name_dups'
    with duppeople.read_transaction as txn:
        for result in duppeople.filter(index, suppress_duplicates=True, txn=txn):
            assert (result.doc._name, result.count) == required_dups_surpressed.pop(0)


def test_filter_suppress_duplicates_page_forward(duppeople, required_dups_surpressed):
    index = 'by_name_dups'
    with duppeople.read_transaction as txn:
        context = None
        while True:
            results = list(duppeople.filter(index, context=context, suppress_duplicates=True, page_size=1, txn=txn))
            if not len(results):
                break
            context = results[-1]
            for result in results:
                assert (result.doc._name, result.count) == required_dups_surpressed.pop(0)


def test_filter_duplicates_page_forward(duppeople, required_dups):
    index = 'by_name_dups'
    with duppeople.read_transaction as txn:
        context = None
        while True:
            results = list(duppeople.filter(index, context=context, page_size=1, txn=txn))
            if not len(results):
                break
            context = results[-1]
            for result in results:
                assert (result.doc._name, result.count) == required_dups.pop(0)


def test_filter_suppress_duplicates_page_backward(duppeople, required_dups_surpressed):
    index = 'by_name_dups'
    with duppeople.read_transaction as txn:
        context = None
        while True:
            results = list(duppeople.filter(index, context=context, page_size=-1, suppress_duplicates=True, txn=txn))
            if not len(results):
                break
            context = results[-1]
            for result in results:
                assert (result.doc._name, result.count) == required_dups_surpressed.pop()


def test_filter_duplicates_page_backward(duppeople, required_dups):
    index = 'by_name_dups'
    with duppeople.read_transaction as txn:
        context = None
        while True:
            results = list(duppeople.filter(index, context=context, page_size=-1, txn=txn))
            if not len(results):
                break
            context = results[-1]
            for result in results:
                assert (result.doc._name, result.count) == required_dups.pop()

def test_filter_interger_key(db):

    data = [
        {'name': 'Tom'},
        {'name': 'Dick'},
        {'name': 'Harry'},
    ]

    people = db.table('people', integerkey=True)
    people.ensure('by_name', '{name}')
    jid = 0
    with WriteTransaction(db) as txn:
        for person in data:
            people.append(Doc(person, jid), txn=txn)
            jid += 1

    jid = 0
    for result in people.filter(lower=Doc(None, pack('>Q', 0))):
        assert result.doc.key == jid
        jid += 1
