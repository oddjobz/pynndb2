import pytest
from pynndb import Manager, Doc, WriteTransaction, SerialiserType, InvalidSerialiser
from shutil import rmtree

DATABASE = '.database'
SERIALISERS = [
    SerialiserType.UJSON,
    SerialiserType.ORJSON,
    SerialiserType.JSON,
    SerialiserType.NONE
]


@pytest.fixture
def required():
    return [
        ('Tom', 2),
        ('Tom', 2),
        ('Dick', 1),
        ('Harry', 1),
        ('Sam', 2),
        ('Sam', 2),
        ('Sally', 1),
        ('Peter', 1),
        ('Jane', 3),
        ('Jane', 3),
        ('Jane', 3),
        ('Fred', 1),
    ]


@pytest.fixture
def data():
    return [
        {'name': 'Tom', 'age': 20},
        {'name': 'Tom', 'age': 21},
        {'name': 'Dick', 'age': 30},
        {'name': 'Harry', 'age': 25},
        {'name': 'Sam', 'age': 35},
        {'name': 'Sam', 'age': 25},
        {'name': 'Sally', 'age': 21},
        {'name': 'Peter', 'age': 11},
        {'name': 'Jane', 'age': 11},
        {'name': 'Jane', 'age': 12},
        {'name': 'Jane', 'age': 10},
        {'name': 'Fred', 'age': 75},
    ]


@pytest.fixture
def db():
    rmtree(DATABASE, ignore_errors=True)
    db = Manager()['mydb'].open(DATABASE)
    yield db
    db.close()


@pytest.fixture(params=SERIALISERS)
def codec(request):
    return request.param


@pytest.fixture
def people(data, db, codec):
    people = db.table('people', codec=codec)
    people.ensure('by_name_dups', '{name}', duplicates=True)
    with WriteTransaction(db) as txn:
        for person in data:
            people.append(Doc(person), txn=txn)
    yield people


def test_filter_primary_check(people, required):
    with people.read_transaction as txn:
        assert [doc.doc._name for doc in people.filter(txn=txn)] == [item[0] for item in required]


def test_basic(db, codec, data):
    people = db.table('people', codec=codec)
    people.ensure('by_name_dups', '{name}', duplicates=True)
    with WriteTransaction(db) as txn:
        for person in data:
            people.append(Doc(person), txn=txn)


def test_sentinel1(db, data):
    people = db.table('people', codec=SerialiserType.UJSON)
    people.ensure('by_name_dups', '{name}', duplicates=True)
    with WriteTransaction(db) as txn:
        for person in data:
            people.append(Doc(person), txn=txn)
    people.close()
    with pytest.raises(InvalidSerialiser):
        people = db.table('people', codec=SerialiserType.ORJSON)

    assert people.codec == SerialiserType.ORJSON


def test_sentinel2(db, data):
    people = db.table('people', codec=SerialiserType.ORJSON)
    people.ensure('by_name_dups', '{name}', duplicates=True)
    with WriteTransaction(db) as txn:
        for person in data:
            people.append(Doc(person), txn=txn)
    people.close()
    with pytest.raises(InvalidSerialiser):
        people = db.table('people', codec=SerialiserType.UJSON)

    assert people.codec == SerialiserType.UJSON


def test_sentinel3(db, data):
    people = db.table('people', codec=SerialiserType.UJSON)
    people.ensure('by_name_dups', '{name}', duplicates=True)
    with WriteTransaction(db) as txn:
        for person in data:
            people.append(Doc(person), txn=txn)
    people.close()
    with pytest.raises(InvalidSerialiser):
        people = db.table('people', codec=SerialiserType.JSON)

    assert people.codec == SerialiserType.JSON
