# PYNNDB2

This is the next generation of the PYNNDB library, currently it is still under development so firstly, expect
it to change, and secondly, don't be surprised if there are a few edge-case bugs knocking around, although
the unit tests and associated application I'm writing have already caught many of the 'best' ones :)

### Version 2.2.8

* Increment POSIX semaphore after creation. Whereas too many 'releases' aren't
  an issue, apparently on creation or re-open, the OS seems to 'eat' the first
  one. Until I can see where this is coming from, just release on open so there's
  a spare in the system. Edge case: first record in a db doesn't get replicated
  until you add a second.

### Version 2.2.7

* More tidy-ups for dynamic DB resizing

### Version 2.2.6

* Some minor internal tweaks to support replication
* Added some optional logging for dynamic DB resizing
* Note; importlib 1.2 is broken, pinning to 1.7

### Version 2.2.5

* Fixes a bunch of regressions involving transparent_resize and journal entries
* Changed import paths to fully qualified from relative

### Version 2.2.4

* Changes relating to replication, internal only
* Bugfix for 2.2.3 (missed a bit)
* Bugfix for 2.2.2 (missed a bit)
* Fixes to 'metadata' to make the routines safe for 'transparent_resize'

### Version 2.2.0

* Adding support for external mesh replication service
* Dynamic resizing code to alter the map size at the DB expands
* Off by default, add conf={'replication': True} when opening DB to activate
* Various small tweaks to support journaling
* Changes to unit tests to complete coverage

### Version 2.1.3

* Adding support for auto-extending the current map size (max db size)
  - this is done "on the fly"
  - transparent so long as you stick to the recommended patterns
  - multi-processing notifications mean multiple process can extend the db simultaneously
  - secondary processes can detect db size changes automatically
  - applications can continue to run uninterrupted after hitting MDB_FULL
  - copes with edge cases like running out of space during a forced re-index while opening a table

### Version 2.1.2

* Moving to pluggable serialisation / deserialisation
* Adding support for URJSON (https://github.com/ijl/orjson)
* Adding support for native JSON

### Version 2.1.1

* Fixed bug in ObjectId and ability to instantiate from a hex string

### Version 2.1.0

* WARNING: ** Potentially Breaking Change **
    Moved to home-grown version of ObjectId
    - needed for (ns) time resolution for multiprocessing
    - 3x faster then generic BSON method
    [this will now use a 28 digit objectid with a different format]

### Version 2.0.11

* Added "storage_used()" to table.py
* Added "storage_used()" to database.py
* Added "storage_allocated" to database.py

### Version 2.0.10

* Added "index" parameter to first() and last() to enable first/last based on index as well as natural order
* Added "filter", this is "big" and is designed to replace "range" (and "seek"), notably handles "paging"

### Version 2.0.9

* Fix the way "tail" behaves, an unknown key will now seek to the start of the table
  - this is primarily to deal with an "empty()", which did render tail stateless

### Version 2.0.8

* Changed the way that "seek" works to correctly return matching keys only for unique indecies
* Added "limit" to "range"
* Adapted range to handle multiple attribute indexes
* Added option to "append" to optionally ignore the "append" mode in LMDB
* Added new test cases
* Allow database path to default to the name in database()

### Version 2.0.3

* First version that looks like it ticks all the 'usable' boxes
* Functional documentation

### Major differences from V1.x

* We now read/write "Doc" objects rather than "dict" objects
* Attributes can now be referenced by index, or by name, to use "by name" each attribute needs to be prefixed
  with an underscore
    ```python
    # attribute reference example
    doc = Doc({'name': 'Fred', 'age': 50})
    print(f'Name={doc["name"]}')
    print(f'Name={doc._name}')
    ```
* There is no longer an ["\_id"] field to represent the primary key, instead the returned "Doc" object has a "key" field, so use "Doc().key". So to extend the above example, as before once a new record is created, the new primary key is left in the "key" field (as opposed to '\_id') in much the same way MySQL provides "last_insert_id()"
    ```python
    # once we have a "doc" instance ..
    table.append(doc)
    print(f'Primary key is: {doc.key}')
    ```
* The top-level abstraction is now "Manager", whic is used to manage multiple databases, but once you get past
this the hierarchy is pretty much the same. Databases, Tables and Indexes are now managed as enumerations to
make things a little easier to handle, but aliases still exist for backwards compatibility
    ```python
    # Setup ...
    from pynndb import Manager, Doc
    manager = Manager()
    db1 = manager.database('.database1')
    db2 = manager.database('.database2')
    people1 = db1.table('people')
    people2 = db2.table('people')
    ```
* Range when used with "keyonly" now returns a "pynndb.Cursor" object rather than an lmdb.Cursor object, this has
attributes "key" and "val" which should return decoded objects. Passing back bytes() from any low level routines
tends to cause confusion so I'm attempting to sanitise all returns
* In general functions should consistently return raw numbers rather than something mangled into human readable form
* "table.records()" returns the number of records in a table "table['index_name'].records()" returns the number of entries in the index described by "index_name"
* The use of transactions should now be consistent and supported up to table / index create level. This is important when launching a number of processes in parallel which may all want to open/create indexes at the same time. In this instance, run setup sequences within a writable transaction like so;
    ```python
    # Ensure another process doesn't try to reindex while we're setting up
    def open(self):
        manager = Manager()
        self._db = manager.database('.myimap')
        with self._db.write_transaction as txn:
            self._folders = self._db.table('folders', txn=txn)
            self._messages = self._db.table('messages', txn=txn)
            self._data = self._db.table('data', txn=txn)
    ```
* both "Database" and "Table" objects provide "read_transaction" and "write_transaction" shortcuts, this is purely for ease of use, note that "lmdb" transactions are always Database centric.