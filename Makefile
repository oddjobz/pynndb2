VERSION := $(shell grep __version__ pynndb/__init__.py |cut -d"'" -f2)
DOCKER_VERSION := 0.0.5

all:
	@echo "version apidocs uploadtest upload deploydocs"

version:
	curl https://img.shields.io/badge/Version-${VERSION}-teal?style=flat -o docs/assets/images/version.svg

apidocs:
	@PYTHONPATH=. ./docs/makeDocs.py pynndb
	@PYTHONPATH=. ./docs/makeDocs.py ../pynndb2-examples/bulk-import
	@PYTHONPATH=. ./docs/makeDocs.py ../pynndb2-examples/people
	@PYTHONPATH=. ./docs/makeDocs.py ../pynndb2-examples/benchmark

uploadtest:
	rm dist/*
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload --repository pypitest dist/*

upload:
	rm dist/*
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload --repository pypi dist/*

deploydocs: apidocs
	rsync -rav docs/* root@legacy:/var/www/pynndb/

